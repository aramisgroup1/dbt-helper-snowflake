const tableRegExp = /(?<prefix>FROM|(?:LEFT|RIGHT|INNER|OUTER)? JOIN) (?<table>[A-Za-z0-9\.\`\'_]+)/gi;
const MODEL = 'model', SOURCE = 'source';

function cleanSQLName(table) {
    return table.replaceAll(/\`/g, "");
}

function findDbtObject(table, models) {
    const cleanedTable = cleanSQLName(table);
    const [name, source] = cleanedTable.split(".").reverse();
    const results = Object.entries(models).filter(([key, model]) => model.schema === source && model.alias === name);

    if (results.length === 0) return {}

    const [[key, value]] = results;

    return value;
}

function replaceSQL(text, models) {
    let replacedText = (' ' + text).slice(1);
    // console.log('Initial value of replacedText \n '+replacedText);
    const output = replacedText.replace(tableRegExp, (match, p1, p2, offset, string, groups) => {
        const { table, prefix } = groups;
        // console.log('Table prefix detected : '+table);
        const dbtObject = findDbtObject(table, models);
        // console.log('dbtObject found in string : '+dbtObject.package_name + ' '+dbtObject.schema+' '+dbtObject.name);
        if (dbtObject.resource_type === MODEL) {
            return `${prefix} {{ ref('${dbtObject.package_name}','${dbtObject.name}') }}`
        } else if(dbtObject.resource_type === SOURCE) {
            return `${prefix} {{ source('${dbtObject.schema}', '${dbtObject.name}') }}`
        } else {
            return match;
        }
    });

    return output;
}